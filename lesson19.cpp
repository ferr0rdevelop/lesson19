#include <iostream>

class Animal
{
public:
    virtual void Voice() 
    {
        std::cout << "Animal voice.";
    }
};

class Dog :  public Animal
{
    void Voice() override
    {
        std::cout << "Woof, woof!\n";
    }
};

class Cat : public Animal
{
    void Voice() override
    {
        std::cout << "Meow...\n";
    }
};

class Cow : public Animal
{
    void Voice() override
    {
        std::cout << "Moo... Moo...\n";
    }
};

int main()
{
    const int size = 3;
    Animal* animals[size];
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Cow();
    for (int i = 0; i < size; i++)
    {
        animals[i]->Voice();
    }
    
}